const mysql2 = require("mysql2");
const {config} = require("dotenv");

config();

const connection = mysql2.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    port: process.env.DB_PORT,
    database: process.env.DB_NAME,
});

connection.connect( (err) => {
    if (err) {
        console.log("Ocurrió un error al conectar la base de datos" + err);
    } else {
        console.log("Se conectó con exito a la base de datos")
    }
});



module.exports = connection


