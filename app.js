const express = require('express');
const app = express();

app.set('view engine', 'ejs');

app.use(express.urlencoded({extended:false}));
app.use(express(JSON));

app.use('/', require('./router/router'));

app.listen(5500, ()=>{
    console.log('Puerto iniciado');
});

