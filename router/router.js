const express = require('express');
const router = express.Router();
const crud = require('../controllers/crud');
const connection = require('../database/db');

router.get('/', (req, res)=>{
   connection.query('SELECT * FROM alumno', (error, results)=>{
        if(error){
            throw error;
        }
        else
        {
            res.render('index', {results: results});
        }
    });
})

router.get('/views/crear', (req, res)=>{
    res.render('crear');
})

router.get('/views/editar/:matricula', (req,res)=>{
    const matricula = req.params.matricula;
    connection.query('SELECT * FROM alumno WHERE matricula=?',[matricula], (error,results)=>{
        if(error){
            throw error;
        }else{
            res.render('editar', {alumno:results[0]});
        }
    })
});

router.get('/delete/:id', (req,res)=>{
    const id = req.params.id;
    connection.query('DELETE FROM alumno WHERE id = ?',[id], (error,results)=>{
        if(error){
            throw error;
        }else{
            res.redirect('/');
        }
    })
});

router.post('/save', crud.save);

router.post('/update', crud.update);

module.exports = router;
