const connection = require('../database/db')

exports.save = ((req, res) =>{
    const nombre = req.body.nombre;
    const matricula = req.body.matricula;
    const domicilio = req.body.domicilio;
    const sexo = req.body.sexo;
    const especialidad = req.body.especialidad;
    connection.query('INSERT INTO alumno SET ?', {nombre, matricula, domicilio, sexo, especialidad}, (error, results)=>{
        if(error){
            console.log(error);
        }else{
            res.redirect('/');
        }
    })

})

exports.update = ((req, res)=>{
    const id = req.body.id;
    const nombre = req.body.nombre;
    const matricula = req.body.matricula;
    const domicilio = req.body.domicilio;
    const sexo = req.body.sexo;
    const especialidad = req.body.especialidad;
    connection.query('UPDATE alumno SET ? WHERE matricula = ?', [{nombre:nombre, matricula:matricula, domicilio:domicilio, sexo:sexo, especialidad:especialidad}, matricula], (error, results)=>{
        if(error){
            console.log(error);
        }else{
            res.redirect('/');
        }
    })
});